'use client';

import ArticleDetail from '@/components/detail-modal';
import FilterComponent from '@/components/filter';
import NoDataComponent from '@/components/no-data';
import Pagination from '@/components/pagination';
import SortByComponent from '@/components/sort';
import { convertTimeAgo } from '@/utils/utils';
import Image from 'next/image';
import { useEffect, useState } from 'react';

export default function Home() {
  const [data, setData] = useState([]);
  const [detail, setDetail] = useState(null);
  const [active, setActive] = useState(1);
  const [filter, setFilter] = useState('Top Headlines');
  const [sort, setSort] = useState('relevancy');
  const [value, setValue] = useState({
    startDate: null,
    endDate: null,
  });

  const getData = async () => {
    const key = process.env.myKey;
    const query = await fetch(
      `${process.env.baseUrl}/top-headlines?country=id&apiKey=${key}`
    );
    const response = await query.json();
    setData(response?.articles);
  };

  const getDataByKeyword = async (page, value, date, sortBy) => {
    const key = process.env.myKey;
    const query = await fetch(
      `${process.env.baseUrl}/everything?q=${value}&sortBy=${sortBy}&from=${date?.startDate}&to=${date?.endDate}&page=${page}&pageSize=10&apiKey=${key}`
    );
    const response = await query.json();
    setData(response?.articles);
  };

  useEffect(() => {
    if (filter === 'Top Headlines') {
      getData();
    }
  }, [filter]);

  return (
    <>
      <FilterComponent
        setFilter={setFilter}
        setActive={setActive}
        getDataByKeyword={getDataByKeyword}
        value={value}
        filter={filter}
        setValue={setValue}
        getData={getData}
        sort={sort}
        setSort={setSort}
      />
      {!data?.length ? (
        <NoDataComponent />
      ) : (
        <div className='container mx-auto px-10 py-10'>
          {filter === 'Top Headlines' && (
            <div className='bg-white rounded-xl shadow-xl p-5 mb-10'>
              <div
                className='flex w-full hover:cursor-pointer'
                onClick={() => setDetail(data?.[0])}
              >
                <div className='w-1/2'>
                  <Image
                    src={
                      data?.[0]?.urlToImage
                        ? data?.[0]?.urlToImage
                        : '/assets/no-image.png'
                    }
                    width={500}
                    height={500}
                    alt=''
                    className='rounded-xl w-full'
                  />
                </div>
                <div className='pl-5 w-1/2'>
                  <p className='text-dark font-bold'>{data?.[0]?.author}</p>
                  <p className='mb-5 text-[grey]'>
                    {convertTimeAgo(data?.[0]?.publishedAt)}
                  </p>
                  <h1 className='text-3xl font-bold text-black'>
                    {data?.[0]?.title}
                  </h1>
                  <p className='mt-5'>{data?.[0]?.description}</p>
                </div>
              </div>
              <hr className='my-5 border-1 border-dark' />
              <div className='grid grid-cols-3 gap-6'>
                {data?.slice(1, 4)?.map((el, i) => (
                  <div
                    key={i}
                    className='hover:cursor-pointer'
                    onClick={() => setDetail(el)}
                  >
                    <div className='h-[200px]'>
                      <Image
                        src={
                          el?.urlToImage
                            ? el?.urlToImage
                            : '/assets/no-image.png'
                        }
                        width={500}
                        height={500}
                        alt=''
                        className='rounded-xl w-full h-[inherit]'
                      />
                    </div>
                    <div className='px-5'>
                      <p className='text-dark font-bold'>{el?.author}</p>
                      <p className='text-xs mb-3 text-[grey]'>
                        {convertTimeAgo(el?.publishedAt)}
                      </p>
                      <p className='text-md font-bold text-black'>
                        {el?.title}
                      </p>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          )}
          {filter !== 'Top Headlines' && (
            <div className='mb-3 flex justify-end'>
              <SortByComponent
                setSort={setSort}
                getDataByKeyword={getDataByKeyword}
                filter={filter}
                value={value}
                sort={sort}
              />
            </div>
          )}
          <div className='rounded-xl shodow-xl'>
            <div className='bg-dark text-white rounded-t-xl p-5'>
              <p className='text-2xl font-bold capitalize'>{filter}</p>
            </div>
            <div className='bg-white p-5 shadow-xl rounded-b-xl'>
              {data?.map((el, i) => (
                <>
                  <div
                    key={i}
                    className='mb-3 flex w-full hover:cursor-pointer'
                    onClick={() => setDetail(el)}
                  >
                    <div className='w-[30%]'>
                      <Image
                        src={
                          el?.urlToImage
                            ? el?.urlToImage
                            : '/assets/no-image.png'
                        }
                        width={500}
                        height={500}
                        alt=''
                        className='rounded-xl w-full'
                      />
                    </div>
                    <div className='ml-5 w-[70%]'>
                      <p className='text-dark font-bold'>{el?.author}</p>
                      <p className='mb-5 text-[grey]'>
                        {convertTimeAgo(el?.publishedAt)}
                      </p>
                      <h1 className='text-3xl font-bold text-black'>
                        {el?.title}
                      </h1>
                      <p className='mt-5'>{el?.description}</p>
                    </div>
                  </div>
                  {data?.length === i + 1 ? (
                    ''
                  ) : (
                    <hr className='border-dark my-5' />
                  )}
                </>
              ))}
            </div>
            {filter !== 'Top Headlines' && (
              <div className='mt-10 flex justify-end'>
                <Pagination
                  active={active}
                  setActive={setActive}
                  getData={(e) => getDataByKeyword(e, filter, value, sort)}
                />
              </div>
            )}
          </div>
          {detail ? (
            <ArticleDetail detail={detail} close={() => setDetail(null)} />
          ) : (
            ''
          )}
        </div>
      )}
    </>
  );
}
