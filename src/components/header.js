const Header = () => {
  return (
    <div className='w-full bg-primary h-[70px]'>
      <div className='flex items-center h-full w-full container mx-auto px-10'>
        <p className='text-black text-xl font-bold'>News App</p>
      </div>
    </div>
  );
};

export default Header;
