import Image from 'next/image';

const Article = ({ article }) => {
  return (
    <div className='border bg-white rounded-xl'>
      <div className='w-full h-[250px]'>
        {article?.urlToImage ? (
          <Image
            src={article?.urlToImage}
            width={300}
            height={250}
            alt=''
            className='h-[inherit] rounded-t-xl'
          />
        ) : null}
      </div>
      <div className='px-5'>
        <p>{article?.author}</p>
        <p>{article?.title}</p>
        <p>{article?.description}</p>
        <p>{article?.publishedAt}</p>
      </div>
    </div>
  );
};

export default Article;
