import { useState } from 'react';

const Pagination = ({ active, setActive, getData }) => {
  const getItemProps = (index) => ({
    className: `border border-dark px-3 py-2 rounded-lg text-dark hover:bg-light ${
      active === index ? 'bg-dark text-white hover:text-dark' : ''
    }
      `,
    onClick: () => {
      setActive(index);
      getData(index);
    },
  });

  const next = () => {
    if (active === 10) return;

    setActive(active + 1);
    getData(active + 1);
  };

  const prev = () => {
    if (active === 1) return;

    setActive(active - 1);
    getData(active - 1);
  };
  return (
    <div className='flex items-center gap-4'>
      <button
        className='flex items-center text-dark hover:cursor-pointer'
        onClick={prev}
        disabled={active === 1}
      >
        Previous
      </button>
      <div className='flex items-center gap-2'>
        {[...Array(5)]?.map((el, i) => (
          <button
            key={i}
            {...getItemProps(active > 5 ? active + 1 + i - 5 : i + 1)}
          >
            {active > 5 ? active + 1 + i - 5 : i + 1}
          </button>
        ))}
      </div>
      <button
        variant='text'
        className='flex items-center text-dark hover:cursor-pointer'
        onClick={next}
        disabled={active === 10}
      >
        Next
      </button>
    </div>
  );
};

export default Pagination;
