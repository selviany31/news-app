import Image from 'next/image';

const NoDataComponent = () => {
  return (
    <div className='w-full h-[80vh] flex justify-center items-center'>
      <Image src='/assets/no-data.svg' width={500} height={500} alt='' />
    </div>
  );
};

export default NoDataComponent;
