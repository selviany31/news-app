import { convertTimeAgo } from '@/utils/utils';
import Image from 'next/image';
import Link from 'next/link';

const ArticleDetail = ({ detail, close }) => {
  return (
    <div className='fixed top-0 left-0 w-full h-full flex justify-center items-center bg-white bg-opacity-70'>
      <div className='max-w-[80%] max-h-[90vh] bg-white shadow-xl py-2 rounded-md overflow-y-auto overflow-x-hidden'>
        <div className='p-10 pb-4'>
          <div>
            <p className='text-3xl font-bold text-black mb-5'>
              {detail?.title}
            </p>
            <p className='text-lg font-semibold my-5'>{detail?.description}</p>
            <p className='text-lg font-bold'>
              By{' '}
              <span className='font-bold text-dark text-lg'>
                {detail?.author}
              </span>
            </p>
            <p className='text-[grey]'>{convertTimeAgo(detail?.publishedAt)}</p>
          </div>
          <div className='w-full mt-5'>
            <Image
              src={
                detail?.urlToImage ? detail?.urlToImage : '/assets/no-image.png'
              }
              width={500}
              height={500}
              alt=''
              className='w-[inherit]'
            />
          </div>
          <div className='mt-5'>
            <p className='mt-5'>{detail?.content}</p>
            <p className='mt-5'>
              {' '}
              More detail:{' '}
              <Link
                href={detail?.url}
                target='_blank'
                className='text-primary hover:text-decoration-line hover:cursor-pointer max-w-full break-all hover:underline'
              >
                {detail?.url}
              </Link>
            </p>
          </div>
        </div>
        <div className='border-t border-gray-300 flex justify-end items-center px-4 pt-2'>
          <button
            type='button'
            className='h-8 px-5 text-sm rounded-md bg-primary text-white hover:bg-dark'
            onClick={close}
          >
            Close
          </button>
        </div>
      </div>
    </div>
  );
};

export default ArticleDetail;
