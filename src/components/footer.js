const Footer = () => {
  return (
    <div className='w-full bg-black h-[70px]'>
      <div className='flex items-center h-full w-full container mx-auto px-10'>
        <p className='text-white text-sm font-bold'>
          Copyright @ 2024 News App
        </p>
      </div>
    </div>
  );
};

export default Footer;
