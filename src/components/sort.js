const SortByComponent = ({
  setSort,
  getDataByKeyword,
  filter,
  value,
  sort,
}) => {
  return (
    <div className='w-[20%]'>
      <select
        className='py-2 px-3 rounded-lg w-full focus:outline-none'
        value={sort}
        onChange={(e) => {
          setSort(e.target.value);
          getDataByKeyword(1, filter, value, e.target.value);
        }}
      >
        <option value={'relevancy'}>Relevancy</option>
        <option value={'popularity'}>Popularity</option>
        <option value={'publishedAt'}>Newest</option>
      </select>
    </div>
  );
};

export default SortByComponent;
