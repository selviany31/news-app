import Datepicker from 'react-tailwindcss-datepicker';

const FilterComponent = ({
  setFilter,
  setActive,
  getDataByKeyword,
  value,
  filter,
  setValue,
  getData,
  sort,
  setSort,
}) => {
  const handleValueChange = (newValue) => {
    setValue(newValue);
    setActive(1);
    getDataByKeyword(1, filter, newValue, sort);
  };

  const handleSubmit = (e) => {
    if (e.key === 'Enter') {
      setFilter(e.target.value);
      setSort('relevancy');
      getDataByKeyword(1, e.target.value, value, 'relevancy');
    }
    if (e.target.value === '') {
      setFilter('Top Headlines');
      setValue({
        startDate: null,
        endDate: null,
      });
    }
  };

  return (
    <div className='w-full bg-dark'>
      <div className='container mx-auto px-10 flex justify-between items-center'>
        <div>
          <button
            className={`text-white px-5 py-3 hover:bg-[#4f8785] ${
              filter === 'Top Headlines' ? 'bg-[#4f8785]' : ''
            }`}
            onClick={() => {
              setFilter('Top Headlines');
              setValue({
                startDate: null,
                endDate: null,
              });
              getData();
            }}
          >
            Top Headlines
          </button>
        </div>
        <div className='flex w-1/2'>
          <div className='flex items-center border rounded-md mr-3 bg-white py-1'>
            <svg
              xmlns='http://www.w3.org/2000/svg'
              viewBox='0 0 20 20'
              fill='grey'
              className='h-5 w-5 ml-2'
            >
              <path
                fillRule='evenodd'
                d='M9 3.5a5.5 5.5 0 100 11 5.5 5.5 0 000-11zM2 9a7 7 0 1112.452 4.391l3.328 3.329a.75.75 0 11-1.06 1.06l-3.329-3.328A7 7 0 012 9z'
                clipRule='evenodd'
              />
            </svg>
            <input
              className='focus:border-0 focus:outline-none ml-2'
              placeholder='Search Keyword'
              type='text'
              onKeyDown={handleSubmit}
            />
          </div>
          <Datepicker
            primaryColor={'teal'}
            inputClassName='w-full hover:border-0 rounded-md ring-2 ring-white py-1 px-5 text-white focus:ring-0 focus:border-0 font-normal bg-[transparent] focus:outline-white'
            toggleClassName='absolute text-white right-0 h-full px-3 text-gray-400 focus:outline-none disabled:opacity-40 disabled:cursor-not-allowed'
            value={value}
            onChange={(e) => handleValueChange(e)}
            displayFormat={'DD/MM/YYYY'}
            disabled={filter === 'Top Headlines'}
          />
        </div>
      </div>
    </div>
  );
};

export default FilterComponent;
