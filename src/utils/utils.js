import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';

export function formatDateTime(value, format) {
  const dateTime = new Date(value);
  if (!value) return '-';
  if (format === 'dd/mm/yyyy') {
    return new Intl.DateTimeFormat('id').format(dateTime);
  }
  if (format === 'dd mmm yyyy hh:mm') {
    return new Intl.DateTimeFormat('id', {
      day: 'numeric',
      month: 'short',
      year: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
    }).format(dateTime);
  }
}

export function convertTimeAgo(value) {
  dayjs.extend(relativeTime);
  if (
    formatDateTime(value, 'dd/mm/yyyy') ===
    formatDateTime(new Date(), 'dd/mm/yyyy')
  ) {
    return dayjs(value).fromNow();
  }
  return formatDateTime(value, 'dd mmm yyyy hh:mm');
}
