/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: '**',
      },
    ],
  },
  env: {
    myKey: process.env.API_KEY,
    baseUrl: process.env.APP_URL,
  },
};

export default nextConfig;
